package com.feed.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.feed.domain.Post;;

@Repository
public interface PostDao  extends JpaRepository<Post, Long>{

}
