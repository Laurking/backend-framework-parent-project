package com.feed.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.feed.domain.Video;
@Repository
public interface VideoDao extends JpaRepository<Video, Long> {

}
