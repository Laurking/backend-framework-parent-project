package com.feed.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.feed.domain.Comment;

@Repository
public interface CommentDao extends JpaRepository<Comment, Long>{

}
