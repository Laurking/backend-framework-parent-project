package com.feed.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.feed.domain.Reply;;

@Repository
public interface ReplyDao  extends JpaRepository<Reply, Long>{

}
