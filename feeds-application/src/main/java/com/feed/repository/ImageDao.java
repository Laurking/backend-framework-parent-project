package com.feed.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.feed.domain.Image;
@Repository
public interface ImageDao extends JpaRepository<Image, Long> {

}
