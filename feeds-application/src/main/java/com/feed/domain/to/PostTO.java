package com.feed.domain.to;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import com.feed.domain.Comment;
import com.feed.domain.Image;
import com.feed.domain.Video;
import com.id.manager.domain.Avatar;
import com.id.manager.domain.User;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Post")
public class PostTO {
	
	@ApiModelProperty(value = "Id for postTO",required = true, position = 1)
	private Long id;
	
	@ApiModelProperty(value = "Body for postTO", required = true, position = 2)
	private String body;
	
	@ApiModelProperty(value = "User info for postTO", required = true, position = 3)
	private User user;
	
	@ApiModelProperty(value = "avatar info for postTO", required = true, position = 4)
	private Avatar avatar;
	
	@ApiModelProperty(value = "Post creation date", required = true, position = 5)
    private Timestamp createdOn;
	
	@ApiModelProperty(value = "Post update date", required = true, position = 6)
    private Timestamp updatedOn;
	
	@ApiModelProperty(value = "Post images", required = true, position = 7)
	private List<Image> images = new ArrayList<Image>();
	
	@ApiModelProperty(value = "User info for postTO", required = true, position = 8)
	private Video video;
	
	@ApiModelProperty(value = "User info for postTO", required = true, position = 9)
	private List<Comment> comments;
	
	public PostTO() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Avatar getAvatar() {
		return avatar;
	}

	public void setAvatar(Avatar avatar) {
		this.avatar = avatar;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

}
