package com.feed.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.id.manager.domain.Avatar;
import com.id.manager.domain.User;

@Entity
@JsonIgnoreProperties(ignoreUnknown=true)
public class Comment {
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String body;
	private Long userId;
	@NotNull
    private Timestamp createdOn;
    @NotNull
    private Timestamp updatedOn;
    @Transient
	private Avatar avatar;
	@Transient
	private User user;
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private Post post;
    @OneToMany(mappedBy = "comment", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonManagedReference
	private List<Reply> replies = new ArrayList<Reply>();
    
    public Comment(){
    	
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}


	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public List<Reply> getReplies() {
		return replies;
	}

	public void setReplies(List<Reply> replies) {
		this.replies = replies;
	}

	public Avatar getAvatar() {
		return avatar;
	}

	public void setAvatar(Avatar avatar) {
		this.avatar = avatar;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", body=" + body + ", userId=" + userId + ", createdOn=" + createdOn
				+ ", updatedOn=" + updatedOn + ", avatar=" + avatar + ", user=" + user + ", post=" + post + ", replies="
				+ replies + "]";
	}
	
	
	
}
