package com.feed.service;


import org.springframework.stereotype.Service;

import com.feed.domain.Video;;

@Service
public interface VideoService {

	public Video createVideo(final Long postId, final Video video);
	public Video getVideoById(final Long id);
	public Video updateVideo(final Video video);
	public Video deleteVideoById(final Long videoId);
}
