package com.feed.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.feed.domain.Image;
import com.feed.domain.Post;
import com.feed.repository.ImageDao;
import com.feed.service.ImageService;
import com.feed.service.PostService;

@Service
public class ImageServiceImplementation implements ImageService {
	@Autowired
	private PostService postService;
	
	@Autowired
	private ImageDao ImageDao;
	
	@Override
	public Image createImage(final Long postId, final Image newImage) {
		final Post post = postService.getPostById(postId);
		newImage.setPost(post);
		final Image image = this.ImageDao.save(newImage);
		return image;
	}


	@Override
	public Image getImageById(Long id) {
		Image image = this.ImageDao.findOne(id);
		return image;
	}

	@Override
	public Image updateImage(Image newImage) {
		Image image = this.ImageDao.save(newImage);
		return image;
	}

	@Override
	public Image deleteImageById(Long imageId) {
		Image image = this.ImageDao.findOne(imageId);
		if(image != null) {
			this.ImageDao.delete(image);
		}
		return image;
	}

}
