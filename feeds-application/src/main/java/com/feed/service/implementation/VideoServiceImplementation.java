package com.feed.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.feed.domain.Post;
import com.feed.domain.Video;
import com.feed.repository.VideoDao;
import com.feed.service.PostService;
import com.feed.service.VideoService;

@Service
public class VideoServiceImplementation implements VideoService {
	@Autowired
	private VideoDao videoDao;
	
	@Autowired
	private PostService postService;
	
	@Override
	public Video createVideo(final Long postId, final Video newVideo) {
		final Post post = this.postService.getPostById(postId);
		newVideo.setPost(post);
		final Video video = this.videoDao.save(newVideo);
		return video;
	}

	@Override
	public Video getVideoById(Long id) {
		Video video = this.videoDao.findOne(id);
		return video;
	}

	@Override
	public Video updateVideo(Video newVideo) {
		Video video = this.videoDao.save(newVideo);
		return video;
	}

	@Override
	public Video deleteVideoById(Long videoId) {
		Video video = this.videoDao.findOne(videoId);
		if(video != null) {
			this.videoDao.delete(video);
		}
		
		return video;
	}

}
