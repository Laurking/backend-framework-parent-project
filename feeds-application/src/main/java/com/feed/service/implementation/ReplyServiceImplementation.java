package com.feed.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.feed.domain.Reply;
import com.feed.repository.ReplyDao;
import com.feed.service.ReplyService;

@Service
public class ReplyServiceImplementation implements ReplyService{
	
	@Autowired
	private ReplyDao replyDao;
	
	@Override
	public Reply createReply(Reply newReply) {
		Reply reply = this.replyDao.save(newReply);
		return reply;
	}

	@Override
	public Reply getReplytById(Long replyId) {
		Reply reply = this.replyDao.findOne(replyId);
		return reply;
	}

	@Override
	public Reply updateReply(Reply newReply) {
		Reply reply = this.replyDao.save(newReply);
		return reply;
	}

	@Override
	public Reply deleteReplyById(Long replyId) {
		Reply reply = this.replyDao.findOne(replyId);
		if(reply != null) {
			this.replyDao.delete(reply);
		}
		return reply;
	}

}
