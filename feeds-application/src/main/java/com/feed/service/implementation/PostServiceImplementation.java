package com.feed.service.implementation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.feed.domain.Comment;
import com.feed.domain.Image;
import com.feed.domain.Post;
import com.feed.domain.Video;
import com.feed.domain.to.PostTO;
import com.feed.repository.PostDao;
import com.feed.service.CommentService;
import com.feed.service.ImageService;
import com.feed.service.PostService;
import com.feed.service.VideoService;
import com.id.manager.domain.Avatar;
import com.id.manager.domain.User;
import com.id.manager.repository.UserRepository;
import com.id.manager.service.AvatarService;

import javassist.tools.rmi.ObjectNotFoundException;

@Service
public class PostServiceImplementation implements PostService {

	@Autowired
	private PostDao postDao;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AvatarService avatarService;
	
	@Autowired
	private VideoService videoService;
	
	
	@Autowired
	private ImageService imageService;
	
	@Autowired 
	private CommentService commentService;
	
	private ModelMapper modelMapper;
	
	public PostServiceImplementation() {
		this.modelMapper = new ModelMapper();
	}
	
	@Override
	public PostTO createPost(Post newPost) {
		final Post post = this.postDao.save(newPost);
		final User user = this.userRepository.findById(post.getId());
		final List<Avatar> avatars = this.avatarService.getAvatarByUserId(post.getUserId());
		final PostTO postTO = modelMapper.map(post, PostTO.class);
		postTO.setUser(user);
		if(avatars.size()>0) {
			Collections.reverse(avatars);
			postTO.setAvatar(avatars.get(0));
		}
		return postTO;
	}

	@Override
	public List<PostTO> getAllPosts() {
		List<PostTO> posts = new ArrayList<PostTO>();
		for (Post post : this.postDao.findAll()) {
			final PostTO  to = modelMapper.map(post, PostTO.class);
			final User user = this.userRepository.findById(post.getUserId());
			final List<Avatar> avatars = this.avatarService.getAvatarByUserId(post.getUserId());
			final List<Comment> comments;
			try {
				comments = this.commentService.getCommentByPostId(post.getId());
				to.setComments(comments);
			} catch (ObjectNotFoundException e) {
				e.printStackTrace();
			}
			
			to.setUser(user);
			if(avatars.size()>0) {
				Collections.reverse(avatars);
				to.setAvatar(avatars.get(0));
			}
			posts.add(to);
		}
		return posts;
		
	}

	@Override
	public Post getPostById(Long postId) {
		Post post = this.postDao.findOne(postId);
		return post;
	}

	@Override
	public Post updatePost(Post newPost) {
		Post post = this.postDao.save(newPost);
		return post;
	}

	@Override
	public Post deletePostById(Long postId) {
		Post post = this.postDao.findOne(postId);
		Video video = post.getVideo();
		List<Image> images = post.getImages();
		List<Comment> comments = post.getComments();
		
		if(video != null) {
			this.videoService.deleteVideoById(video.getId());
		}
		for (Image image : images) {
			this.imageService.deleteImageById(image.getId());
		}
		for (Comment comment : comments) {
			this.commentService.deleteCommentById(comment.getId());
		}
		return post;
		
	}

}
