package com.feed.service.implementation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.feed.domain.Comment;
import com.feed.domain.Reply;
import com.feed.repository.CommentDao;
import com.feed.repository.ReplyDao;
import com.feed.service.CommentService;
import com.id.manager.domain.Avatar;
import com.id.manager.domain.User;
import com.id.manager.service.AvatarService;
import com.id.manager.service.UserService;

import javassist.tools.rmi.ObjectNotFoundException;

@Service
public class CommentServiceImplementation implements CommentService {

	@Autowired
	private CommentDao commentDao;

	@Autowired
	private ReplyDao replyDao;

	@Autowired
	private UserService userService;

	@Autowired
	private AvatarService avatarService;

	@Override
	public Comment createComment(Comment newComment) {
		Comment comment = this.commentDao.save(newComment);
		return comment;
	}

	@Override
	public Comment getCommentById(Long commentId) {
		Comment comment = this.commentDao.findOne(commentId);
		return comment;
	}

	@Override
	public Comment updateComment(Comment newComment) {
		Comment comment = this.commentDao.save(newComment);
		return comment;
	}

	@Override
	public Comment deleteCommentById(Long commentId) {
		Comment comment = this.commentDao.findOne(commentId);
		List<Reply> replies = comment.getReplies();
		if (replies != null) {
			this.replyDao.delete(replies);
		}
		return comment;
	}

	@Override
	public List<Comment> getCommentByPostId(Long postId) throws ObjectNotFoundException {
		final List<Comment> comments = new ArrayList<Comment>();
		for (Comment comment : this.commentDao.findAll()) {
			if (comment.getPost().getId() == postId) {
				final List<Avatar> avatars = this.avatarService.getAvatarByUserId(comment.getUserId());
				final User user = this.userService.findById(comment.getUserId());
				comment.setUser(user);
				if(avatars.size()>0) {
					Collections.reverse(avatars);
					comment.setAvatar(avatars.get(0));
				}
				Collections.reverse(comments);
				comments.add(comment);
			}
		}
		System.out.println(comments);
		return comments;
	}

}
