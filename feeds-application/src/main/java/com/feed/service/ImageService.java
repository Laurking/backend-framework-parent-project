package com.feed.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.feed.domain.Image;;

@Service
public interface ImageService {
	public Image createImage(final Long postId, final Image image);
	public Image getImageById(final Long id);
	public Image updateImage(final Image image);
	public Image deleteImageById(Long imageId);

}
