package com.feed.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.feed.domain.Post;
import com.feed.domain.to.PostTO;

@Service
public interface PostService {
	public PostTO createPost(Post post);
	public List<PostTO> getAllPosts();
	public Post getPostById(Long id);
	public Post updatePost(Post post);
	public Post deletePostById(Long postId);

}
