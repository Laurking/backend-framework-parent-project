package com.feed.service;


import org.springframework.stereotype.Service;

import com.feed.domain.Reply;;

@Service
public interface ReplyService {
	public Reply createReply(Reply newReply);
	public Reply getReplytById(Long replyId);
	public Reply updateReply(Reply newReply);
	public Reply deleteReplyById(Long replyId);

}
