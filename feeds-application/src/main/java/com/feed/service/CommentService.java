package com.feed.service;


import java.util.List;

import org.springframework.stereotype.Service;

import com.feed.domain.Comment;

import javassist.tools.rmi.ObjectNotFoundException;;

@Service
public interface CommentService {
	
	public Comment createComment(Comment comment);
	public Comment getCommentById(Long commentId);
	public Comment updateComment(Comment comment);
	public Comment deleteCommentById(Long commentId);
	public List<Comment> getCommentByPostId(final Long postId) throws ObjectNotFoundException;

}
