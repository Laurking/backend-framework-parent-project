package com.feed.web;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.feed.domain.Comment;
import com.feed.domain.Reply;
import com.feed.service.CommentService;
import com.feed.service.ReplyService;

@RestController
@RequestMapping("/api/v1")
public class ReplyController {
	
	@Autowired
	private ReplyService replyService;
	
	
	@RequestMapping(value = "/comment/{commentId}/reply/add-reply", method = RequestMethod.POST)
	@ResponseBody
	public Reply createReply(@RequestBody Reply newReply) {
		newReply.setCreatedOn(new Timestamp(new Date().getTime()));
		newReply.setUpdatedOn(new Timestamp(new Date().getTime()));
		return this.replyService.createReply(newReply);
	}
	
	@RequestMapping(value = "/comment/{commentId}/reply/{replyId}/read-reply", method = RequestMethod.GET)
	@ResponseBody
	public Reply getReplyById(@PathVariable Long replyId) {
		return this.replyService.getReplytById(replyId);
	}
	
	@RequestMapping(value = "/comment/{commentId}/reply/{replyId}/update-reply", method = RequestMethod.PUT)
	@ResponseBody
	public Reply updateReply(@RequestBody Reply reply) {
		return this.replyService.updateReply(reply);
	}
	
	@RequestMapping(value = "/comment/{commentId}/reply/{replyId}/remove-reply", method = RequestMethod.DELETE)
	@ResponseBody
	public Reply deleteReply(@PathVariable Long replyId) {
		return this.replyService.deleteReplyById(replyId);
	}
	
	
	
	
}
