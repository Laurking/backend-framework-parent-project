package com.feed.web;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.feed.domain.Comment;
import com.feed.service.CommentService;

@RestController
@RequestMapping("/api/v1")
public class CommentController {
	
	@Autowired
	private CommentService commentService;
	
	@RequestMapping(value = "/post/{postId}/comment/add-comment", method = RequestMethod.POST)
	@ResponseBody
	public Comment createComment(@RequestBody Comment comment) {
		comment.setCreatedOn(new Timestamp(new Date().getTime()));
		comment.setUpdatedOn(new Timestamp(new Date().getTime()));
		return this.commentService.createComment(comment);
	}
	
	@RequestMapping(value = "/comment/{commentId}/read-comment", method = RequestMethod.GET)
	@ResponseBody
	public Comment getCommentById(@PathVariable Long commentId) {
		return this.commentService.getCommentById(commentId);
	}
	
	@RequestMapping(value = "/comment/{commentId}/update-comment", method = RequestMethod.PUT)
	@ResponseBody
	public Comment updateComment(@RequestBody Comment comment) {
		return this.commentService.updateComment(comment);
	}
	
	@RequestMapping(value = "/comment/{commentId}/remove-comment", method = RequestMethod.DELETE)
	@ResponseBody
	public Comment deleteCommentById(@PathVariable Long commentId) {
		return this.commentService.deleteCommentById(commentId);
	}
	
}
