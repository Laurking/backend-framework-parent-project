package com.feed.web;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.feed.domain.Image;
import com.feed.service.ImageService;
import com.id.manager.helpers.FileExtensionProcessor;


@RestController
@RequestMapping("/api/v1")
public class ImageController {
	
	@Autowired
	private ImageService imageService;
	
	@RequestMapping(value = "/post/{postId}/image/add-image", method = RequestMethod.POST)
	@ResponseBody
	public Image createImage(@PathVariable Long postId, MultipartFile imageFiles) throws Exception {
		final Image image = new Image();
		final FileExtensionProcessor processor = new FileExtensionProcessor();
		if(!processor.isAValidImage(imageFiles.getOriginalFilename())) {
			
			throw new Exception();
		}
		image.setType(FileExtensionProcessor.getFileExtension());
		image.setFilename(imageFiles.getOriginalFilename());
		image.setSize(imageFiles.getSize());
		image.setCreatedOn(new Timestamp(new Date().getTime()));
		image.setUpdatedOn(new Timestamp(new Date().getTime()));
		image.setSource(imageFiles.getBytes());
		return this.imageService.createImage(postId,image);
		
	}
	
	@RequestMapping(value = "/post/{postId}/image/{imageId}/read-image", method = RequestMethod.GET)
	@ResponseBody
	public Image getImageById(@PathVariable Long imageId) {
		return this.imageService.getImageById(imageId);
	}
	
	
	@RequestMapping(value = "/post/{postId}/image/{imageId}/update-image", method = RequestMethod.PUT)
	@ResponseBody
	public Image updateImage(@RequestBody Image image) {
		return this.imageService.updateImage(image);
	}
	
	@RequestMapping(value = "/post/{postId}/image/{imageId}/remove-image", method = RequestMethod.DELETE)
	@ResponseBody
	public Image deleteImage(@PathVariable Long imageId) {
		return this.imageService.deleteImageById(imageId);
	}

}
