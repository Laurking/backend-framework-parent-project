package com.feed.web;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.feed.domain.Video;
import com.feed.service.VideoService;
import com.id.manager.helpers.FileExtensionProcessor;

@RestController
@RequestMapping("/api/v1")
public class VideoController {

	@Autowired
	private VideoService videoService;
	
	@RequestMapping(value = "/post/{postId}/video/add-video", method = RequestMethod.POST)
	@ResponseBody
	public Video createVideo(@PathVariable Long postId, final MultipartFile videoFile) throws Exception {
		final Video video = new Video();
		final FileExtensionProcessor processor = new FileExtensionProcessor();
		if(processor.isAValidVideo(videoFile.getOriginalFilename())) {
			video.setType(FileExtensionProcessor.getFileExtension());
			video.setCreatedOn(new Timestamp(new Date().getTime()));
			video.setUpdatedOn(new Timestamp(new Date().getTime()));
			video.setFilename(videoFile.getOriginalFilename());
			video.setSize(videoFile.getSize());
			video.setSource(videoFile.getBytes());
		}
		else {
			throw new Exception();
		}
		return this.videoService.createVideo(postId,video);
	}
	
	@RequestMapping(value = "/post/{postId}/video/{videoId}/read-video", method = RequestMethod.GET)
	@ResponseBody
	public Video getVideoById(@PathVariable final Long videoId) {
		return this.videoService.getVideoById(videoId);
	}
	
	@RequestMapping(value = "/post/{postId}/video/{videoId}/update-video", method = RequestMethod.PUT)
	@ResponseBody
	public Video updateVideo(@RequestBody final Video video) {
		return this.videoService.updateVideo(video);
	}
	
	@RequestMapping(value = "/post/{postId}/video/{videoId}/remove-video", method = RequestMethod.DELETE)
	@ResponseBody
	public Video deleteVideo(@PathVariable final Long videoId) {
		return this.videoService.deleteVideoById(videoId);
	}
	
}
