package com.feed.web;

import java.sql.Timestamp;
import java.util.List;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.feed.domain.Post;
import com.feed.domain.to.PostTO;
import com.feed.service.PostService;

@RestController
@RequestMapping("/api/v1")
public class PostController {
	
	@Autowired
	private PostService postService;

	@RequestMapping(value = "/post/add-post", method = RequestMethod.POST)
	@ResponseBody 
	public PostTO createPost(@RequestBody Post post) {
		post.setCreatedOn(new Timestamp(new Date().getTime()));
		post.setUpdatedOn(new Timestamp(new Date().getTime()));
		PostTO saved = this.postService.createPost(post);
		return saved;
	}
	
	@RequestMapping(value = "/post/read-all-posts", method = RequestMethod.GET)
	@ResponseBody 
	public List<PostTO> getAllPosts() {
		return this.postService.getAllPosts();
	}
	
	@RequestMapping(value = "/post/{postId}/read-post", method = RequestMethod.GET)
	@ResponseBody 
	public Post getPostById(@PathVariable Long postId) {
		return this.postService.getPostById(postId);
	}
	
	@RequestMapping(value = "/post/{postId}/update-post", method = RequestMethod.PUT)
	@ResponseBody 
	public Post updatePost(@RequestBody Post newPost) {
		return this.postService.updatePost(newPost);
	}
	
	@RequestMapping(value = "/post/{postId}/remove-post", method = RequestMethod.DELETE)
	@ResponseBody 
	public Post deleatePost(@PathVariable Long postId) {
		return this.postService.deletePostById(postId);
	}
	
	
	
	

}
