package com.marialex.configuration;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.marialex.variables.Constants;

@Component
public class CrossOrginConfig extends OncePerRequestFilter {

    private final String MAX_AGE = "4800";
	private final String XSRF_TOKEN = "xsrf-token";
	private final String ACCESS_CONTROL_EXPOSE_HEADERS = "Access-Control-Expose-Headers";
	private final String AUTHORIZATION_CONTENT_TYPE_XSRF_TOKEN = "authorization, content-type, xsrf-token";
	private final String ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
	private final String ACCESS_CONTROL_MAX_AGE = "Access-Control-Max-Age";
	private final String ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
	private final String OPTIONS = "OPTIONS";
	private final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";

	@Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        response.setHeader(ACCESS_CONTROL_ALLOW_ORIGIN,Constants.getOrginUrl());
        response.setHeader(ACCESS_CONTROL_ALLOW_METHODS, Constants.getRequestMethods());
        response.setHeader(ACCESS_CONTROL_MAX_AGE, MAX_AGE);
        response.setHeader(ACCESS_CONTROL_ALLOW_HEADERS, AUTHORIZATION_CONTENT_TYPE_XSRF_TOKEN);
        response.addHeader(ACCESS_CONTROL_EXPOSE_HEADERS, XSRF_TOKEN);
        if (OPTIONS.equals(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else { 
            filterChain.doFilter(request, response);
        }
    }
}