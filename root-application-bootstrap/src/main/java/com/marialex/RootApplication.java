package com.marialex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication(scanBasePackages={"com.marialex"})
@ComponentScan(basePackages= {"com.feed","com.id","com.video","com.id.manager","com.marialex"})
@EnableJpaRepositories(basePackages= {"com.feed","com.id.manager","com.video"})
@EntityScan(basePackages= {"com.feed","com.id.manager","com.video"})
public class RootApplication
{
    public static void main( String[] args )
    {
    	 ApplicationContext ctx = SpringApplication.run(RootApplication.class, args);
    }
}
