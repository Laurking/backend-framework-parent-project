package com.marialex.variables;

public class Constants {
	private static final String requestOrginUrl = "http://localhost:4200";
	private static final String requestMethods = "GET, POST, PUT, DELETE, OPTIONS";
	
	public static String getOrginUrl() {
		return requestOrginUrl;
	}

	public static String getRequestMethods() {
		return requestMethods;
	}
}
