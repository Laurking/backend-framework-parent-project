package com.id.manager.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileExtensionProcessor {
	private final String[] imageExt = { "png", "jpeg", "jpg", "bmp", "gif" };
	private final String[] videoExt = { "mp4", "mp3", "mp2" };
	private final List<String> imageExtensions = new ArrayList<String>(Arrays.asList(imageExt));
	private final List<String> VideoExtensions = new ArrayList<String>(Arrays.asList(videoExt));
	private static String fileExtension = null;
	
	public boolean isAValidVideo(String filename) {
		filename = filename.toLowerCase();
		if (filename != null && filename.length() > 0) {
			if (filename.contains(".") && filename.lastIndexOf(".") != 0) {
				fileExtension = filename.substring(filename.lastIndexOf(".") + 1);
			}
		}
		return VideoExtensions.contains(fileExtension);
	}

	public boolean isAValidImage(String filename) {
		filename = filename.toLowerCase();
		if (filename != null && filename.length() > 0) {
			if (filename.contains(".") && filename.lastIndexOf(".") != 0) {
				fileExtension = filename.substring(filename.lastIndexOf(".") + 1);
			}
		}
		return imageExtensions.contains(fileExtension);

	}

	public static String getFileExtension() {
		return fileExtension;
	}
	
}
