package com.id.manager.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "User")
public class UserTO {

	@ApiModelProperty(value = "Id for user",required = true, position = 1)
	private Long id;
	
	@ApiModelProperty(value = "first name for user",required = true, position = 2)
	private String firstName;

	@ApiModelProperty(value = "last name for user",required = true, position = 3)
	private String lastName;

	@ApiModelProperty(value = "email for user",required = true, position = 4)
	private String email;

	@ApiModelProperty(value = "is user active",required = true, position = 5)
	private Integer active;

	@ApiModelProperty(value = "user role",required = true, position = 6)
	private Role role;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
}
