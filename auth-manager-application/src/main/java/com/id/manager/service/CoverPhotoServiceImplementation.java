package com.id.manager.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.id.manager.domain.CoverPhoto;
import com.id.manager.repository.CoverPhotoRepository;

@Service("coverPhotoService")
@Transactional
public class CoverPhotoServiceImplementation implements CoverPhotoService{

	@Autowired
	private CoverPhotoRepository coverPhotoRepository;
	
	@Override
	public CoverPhoto createCoverPhoto(CoverPhoto coverPhoto) {
		final CoverPhoto newPhoto = this.coverPhotoRepository.save(coverPhoto);
		return newPhoto;
	}

	@Override
	public CoverPhoto getCoverPhotoById(Long coverPhotoId) {
		final CoverPhoto newPhoto = this.coverPhotoRepository.findOne(coverPhotoId);
		return newPhoto;
	}

	@Override
	public CoverPhoto updateCoverPhoto(CoverPhoto coverPhoto) {
		final CoverPhoto newPhoto = this.coverPhotoRepository.save(coverPhoto);
		return newPhoto;
	}

	@Override
	public CoverPhoto deleteCoverPhotoById(Long coverPhotoId) {
		final CoverPhoto newPhoto = this.coverPhotoRepository.findOne(coverPhotoId);
		if(newPhoto != null) {
			this.coverPhotoRepository.delete(newPhoto);
		}
		return newPhoto;
	}

	@Override
	public List<CoverPhoto> getCoverPhotoByUserId(Long userId) {
		final List<CoverPhoto> coverPhotos = new ArrayList<CoverPhoto>();
		final List<CoverPhoto> dbPhotos = this.coverPhotoRepository.findAll();
		for (CoverPhoto coverPhoto : dbPhotos) {
			if(coverPhoto.getUserId() == userId) {
				coverPhotos.add(coverPhoto);
			}
		}
		return coverPhotos;
	}

	@Override
	public List<CoverPhoto> getAllCoverPhoto() {
		final List<CoverPhoto> photos = this.coverPhotoRepository.findAll();
		return photos;
	}

}
