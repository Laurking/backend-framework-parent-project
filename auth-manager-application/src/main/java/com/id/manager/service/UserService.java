package com.id.manager.service;

import com.id.manager.domain.User;

import javassist.tools.rmi.ObjectNotFoundException;

public interface UserService {
	public User findByEmail(String email);
	public User findById(Long id) throws ObjectNotFoundException;
	public User registerUser(final User user);
	public User registerAdmin(final User user);
}

