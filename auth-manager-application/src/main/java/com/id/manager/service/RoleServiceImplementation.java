package com.id.manager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javassist.tools.rmi.ObjectNotFoundException;
import com.id.manager.domain.Role;
import com.id.manager.repository.RoleRepository;
@Service("roleService")
@Transactional
public class RoleServiceImplementation implements RoleService {

	@Autowired
	private RoleRepository roleRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Role> findAll() throws ObjectNotFoundException {
		
			List<Role> roles = roleRepository.findAll();
			if(roles.isEmpty()){
				throw new ObjectNotFoundException("Role list is empty");
			}
			return roles;

	}

}
