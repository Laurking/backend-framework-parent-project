package com.id.manager.service;

import java.util.List;

import com.id.manager.domain.Avatar;


public interface AvatarService {
	public Avatar createAvatar(Avatar avatar);
	public Avatar getAvatarById(Long avatarId);
	public Avatar updateAvatar(Avatar avatar);
	public Avatar deleteAvatarById(Long avatarId);
	public List<Avatar> getAvatarByUserId(Long userId);
	public List<Avatar> getAllAvatars();
}
