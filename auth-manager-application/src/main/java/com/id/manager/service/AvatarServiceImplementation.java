package com.id.manager.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.id.manager.domain.Avatar;
import com.id.manager.repository.AvatarRepository;

@Service("avatarService")
@Transactional
public class AvatarServiceImplementation implements AvatarService{
	
	@Autowired
	private AvatarRepository avatarRepository;
	
	
	@Override
	public Avatar createAvatar(Avatar avatar) {
		final Avatar newAvatar = this.avatarRepository.save(avatar);
		return newAvatar;
	}

	@Override
	public Avatar getAvatarById(Long avatarId) {
		final Avatar avatar = this.avatarRepository.findOne(avatarId);
		return avatar;
	}

	@Override
	public Avatar updateAvatar(Avatar avatar) {
		final Avatar updatedAvatar = this.avatarRepository.save(avatar);
		return updatedAvatar;
	}
	
	@Override
	public List<Avatar> getAvatarByUserId(Long userId) {
		final List<Avatar> avatars = new ArrayList<Avatar>();
		final List<Avatar> dbAvatars = this.avatarRepository.findAll();
		for (Avatar avatar : dbAvatars) {
			if(avatar.getUserId() == userId) {
				avatars.add(avatar);
			}
		}
		return avatars;
	}

	@Override
	public Avatar deleteAvatarById(Long avatarId) {
		final Avatar avatar = this.avatarRepository.findOne(avatarId);
		if(avatar != null) {
			this.avatarRepository.delete(avatar);
		}
		return avatar;
	}

	@Override
	public List<Avatar> getAllAvatars() {
		final List<Avatar> avatars = new ArrayList<Avatar>();
		this.avatarRepository.findAll().forEach(avatar ->{
			avatars.add(avatar);
		});
		return avatars;
	}

}
