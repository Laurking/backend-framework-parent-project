package com.id.manager.service;

import javax.persistence.NonUniqueResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.id.manager.domain.Role;
import com.id.manager.domain.User;
import com.id.manager.repository.RoleRepository;
import com.id.manager.repository.UserRepository;

import javassist.tools.rmi.ObjectNotFoundException;

@Service("userService")
@Transactional
public class UserServiceImplementation implements UserService {


	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	

	@Override
	@Transactional(readOnly = true)
	public User findByEmail(String email) {
		try {
			User user = userRepository.findByEmail(email);
			return user;
		} catch (Exception e) {
			throw e;
		}

	}

	@Override
	@Transactional(readOnly = true)
	public User findById(Long id) throws ObjectNotFoundException {
		User user = userRepository.findOne(id);
		return user;
		
	}

	@Override
	public User registerUser(User user) {
		final User existingUser = this.userRepository.findByEmail(user.getEmail());
		if(existingUser != null) {
			throw new NonUniqueResultException(String.format("%s already exists",user.getEmail()));
		}
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setActive(1);
		final Role userRole = roleRepository.findByName("ROLE_USER");
		user.setRole(userRole);
		userRepository.save(user);
		return user;
	}

	@Override
	public User registerAdmin(User user) {
		final User existingUser = this.userRepository.findByEmail(user.getEmail());
		if(existingUser != null) {
			throw new NonUniqueResultException(String.format("%s already exists",user.getEmail()));
		}
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setActive(1);
		final Role userRole = roleRepository.findByName("ROLE_ADMIN");
		user.setRole(userRole);
		userRepository.save(user);
		return user;
	}

}
