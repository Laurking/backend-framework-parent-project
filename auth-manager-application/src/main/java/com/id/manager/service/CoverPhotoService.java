package com.id.manager.service;

import java.util.List;

import com.id.manager.domain.CoverPhoto;;

public interface CoverPhotoService {
	public CoverPhoto createCoverPhoto(CoverPhoto coverPhoto);
	public CoverPhoto getCoverPhotoById(Long coverPhotoId);
	public CoverPhoto updateCoverPhoto(CoverPhoto coverPhoto);
	public CoverPhoto deleteCoverPhotoById(Long coverPhotoId);
	public List<CoverPhoto> getCoverPhotoByUserId(Long userId);
	public List<CoverPhoto> getAllCoverPhoto();
}
