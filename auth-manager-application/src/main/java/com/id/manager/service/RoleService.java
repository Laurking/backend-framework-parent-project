package com.id.manager.service;

import java.util.List;

import com.id.manager.domain.Role;

import javassist.tools.rmi.ObjectNotFoundException;

public interface RoleService {
	 List<Role> findAll()throws ObjectNotFoundException;
}
