package com.id.manager.configuration;

public class ErrorResponseMessage {

	private String errorMessage;

	public ErrorResponseMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}
