package com.id.manager.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.id.manager.configuration.Constants;
import com.id.manager.configuration.ErrorResponseMessage;
import com.id.manager.configuration.TokenUtil;
import com.id.manager.domain.MarialexUser;
import com.id.manager.domain.User;
import com.id.manager.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.tools.rmi.ObjectNotFoundException;

@RestController
@RequestMapping("/api/v1")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/user-register", method = { RequestMethod.POST })
	@ApiOperation(value = "register")
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constants.SUCCESS, response = MarialexUser.class),
			@ApiResponse(code = 403, message = Constants.FORBIDDEN),
			@ApiResponse(code = 422, message = Constants.USER_NOT_FOUND),
			@ApiResponse(code = 417, message = Constants.EXCEPTION_FAILED) })
	public ResponseEntity<User> registerUser(@RequestBody User user) {
		final User newUser = this.userService.registerUser(user);
		return new ResponseEntity<User>(newUser, HttpStatus.OK);
	}

	@RequestMapping(value = "/user/admin-register", method = { RequestMethod.POST })
	@ApiOperation(value = "register")
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constants.SUCCESS, response = MarialexUser.class),
			@ApiResponse(code = 403, message = Constants.FORBIDDEN),
			@ApiResponse(code = 422, message = Constants.USER_NOT_FOUND),
			@ApiResponse(code = 417, message = Constants.EXCEPTION_FAILED) })
	public ResponseEntity<User> registerAdmin(@RequestBody User user) {
		final User newUser = this.userService.registerAdmin(user);
		return new ResponseEntity<User>(newUser, HttpStatus.OK);
	}

	@RequestMapping(value = "/user/current-session-details", method = { RequestMethod.POST })
	@ApiOperation(value = "user session")
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constants.SUCCESS, response = User.class),
			@ApiResponse(code = 403, message = Constants.FORBIDDEN),
			@ApiResponse(code = 422, message = Constants.USER_NOT_FOUND),
			@ApiResponse(code = 417, message = Constants.EXCEPTION_FAILED) })
	public ResponseEntity<User> foundActiveSession(HttpServletRequest request, HttpServletResponse response,
			@RequestBody final String token, final Authentication authentication) {
		final String currentToken = TokenUtil.getUserNameFromToken(token.trim());
		final User user = this.userService.findByEmail(currentToken);
		return new ResponseEntity<User>(user, HttpStatus.OK);

	}

	@ApiOperation(value = "Get user by id")
	@RequestMapping(value = "/users/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constants.SUCCESS, response = User.class),
			@ApiResponse(code = 403, message = Constants.FORBIDDEN),
			@ApiResponse(code = 422, message = Constants.USER_NOT_FOUND),
			@ApiResponse(code = 417, message = Constants.EXCEPTION_FAILED) })
	public ResponseEntity<?> getUserById(@PathVariable("userId") Long userId) {

		try {
			User user = userService.findById(userId);
			return new ResponseEntity<User>(user, HttpStatus.OK);

		} catch (ObjectNotFoundException onfe) {
			onfe.printStackTrace();
			ErrorResponseMessage error = new ErrorResponseMessage("User with id = " + userId + " is not found");
			return new ResponseEntity<ErrorResponseMessage>(error, HttpStatus.UNPROCESSABLE_ENTITY);

		} catch (Exception e) {
			e.printStackTrace();
			ErrorResponseMessage error = new ErrorResponseMessage("An error has occured");
			return new ResponseEntity<ErrorResponseMessage>(error, HttpStatus.EXPECTATION_FAILED);
		}

	}

}
