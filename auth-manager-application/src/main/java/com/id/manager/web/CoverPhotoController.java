package com.id.manager.web;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.id.manager.domain.CoverPhoto;
import com.id.manager.helpers.FileExtensionProcessor;
import com.id.manager.service.CoverPhotoService;

@RestController
@RequestMapping("/api/v1")
public class CoverPhotoController {
	
	@Autowired
	private CoverPhotoService coverPhotoService;
	
	@RequestMapping(value = "users/user/{userId}/cover-photo/add", method = RequestMethod.POST)
	@ResponseBody
	 public CoverPhoto createCoverPhoto(@PathVariable Long userId, MultipartFile imageFiles) throws Exception {
		final CoverPhoto coverPhoto = new CoverPhoto();
		final FileExtensionProcessor processor = new FileExtensionProcessor();
		if(!processor.isAValidImage(imageFiles.getOriginalFilename())) {
			throw new Exception();
		}
		coverPhoto.setUserId(userId);
		coverPhoto.setType(FileExtensionProcessor.getFileExtension());
		coverPhoto.setFilename(imageFiles.getOriginalFilename());
		coverPhoto.setSize(imageFiles.getSize());
		coverPhoto.setCreatedOn(new Timestamp(new Date().getTime()));
		coverPhoto.setUpdatedOn(new Timestamp(new Date().getTime()));
		coverPhoto.setSource(imageFiles.getBytes());
		return this.coverPhotoService.createCoverPhoto(coverPhoto);
	}
	
	@RequestMapping(value = "users/user/{userId}/cover-photo/{coverPhotoId}/find", method = RequestMethod.GET)
	@ResponseBody
	public CoverPhoto findCoverPhotoById(@PathVariable Long coverPhotoId) {
		final CoverPhoto coverPhoto = this.coverPhotoService.getCoverPhotoById(coverPhotoId);
		return coverPhoto;
	}
	
	@RequestMapping(value = "users/user/{userId}/cover-photo/findCoverPhotos", method = RequestMethod.GET)
	@ResponseBody
	public List<CoverPhoto> findCoverPhotoByUserId(@PathVariable Long userId) {
		final List<CoverPhoto> coverPhoto = this.coverPhotoService.getCoverPhotoByUserId(userId);
		return coverPhoto == null? null :coverPhoto;
	}
	
	@RequestMapping(value = "users/cover-photos/findall-photos", method = RequestMethod.GET)
	@ResponseBody
	public List<CoverPhoto> findAllCoverPhotos() {
		final List<CoverPhoto> coverPhotos = this.coverPhotoService.getAllCoverPhoto();
		return coverPhotos;
	}
}
