package com.id.manager.web;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.id.manager.domain.Avatar;
import com.id.manager.helpers.FileExtensionProcessor;
import com.id.manager.service.AvatarService;

@RestController
@RequestMapping("/api/v1")
public class AvatarController {
	
	@Autowired
	private AvatarService avatarService;
	
	@RequestMapping(value = "users/user/{userId}/avartar/add", method = RequestMethod.POST)
	@ResponseBody
	 public Avatar createAvatar(@PathVariable Long userId, MultipartFile imageFiles) throws Exception {
		final Avatar avatar = new Avatar();
		final FileExtensionProcessor processor = new FileExtensionProcessor();
		if(!processor.isAValidImage(imageFiles.getOriginalFilename())) {
			throw new Exception();
		}
		avatar.setUserId(userId);
		avatar.setType(FileExtensionProcessor.getFileExtension());
		avatar.setFilename(imageFiles.getOriginalFilename());
		avatar.setSize(imageFiles.getSize());
		avatar.setCreatedOn(new Timestamp(new Date().getTime()));
		avatar.setUpdatedOn(new Timestamp(new Date().getTime()));
		avatar.setSource(imageFiles.getBytes());
		return this.avatarService.createAvatar(avatar);
	}
	
	@RequestMapping(value = "users/user/{userId}/avartar/{avatarId}/find", method = RequestMethod.GET)
	@ResponseBody
	public Avatar findAvatarById(@PathVariable Long avatarId) {
		final Avatar avatar = this.avatarService.getAvatarById(avatarId);
		return avatar;
	}
	
	@RequestMapping(value = "users/user/{userId}/avartar/findAvatars", method = RequestMethod.GET)
	@ResponseBody
	public List<Avatar> findAvatarByUserId(@PathVariable Long userId) {
		final List<Avatar> avatars = this.avatarService.getAvatarByUserId(userId);
		return avatars == null? null :avatars;
	}
	
	@RequestMapping(value = "users/avartar/findall-avatars", method = RequestMethod.GET)
	@ResponseBody
	public List<Avatar> findAllAvatars() {
		final List<Avatar> avatars = this.avatarService.getAllAvatars();
		return avatars;
	}
}
