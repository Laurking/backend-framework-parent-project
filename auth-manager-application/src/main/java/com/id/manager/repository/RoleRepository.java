package com.id.manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.id.manager.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
	public Role findByName(String roleName);
}