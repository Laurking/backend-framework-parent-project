package com.id.manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.id.manager.domain.CoverPhoto;

public interface CoverPhotoRepository extends JpaRepository<CoverPhoto, Long>{

}
