package com.id.manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.id.manager.domain.Avatar;

public interface AvatarRepository extends JpaRepository<Avatar, Long>{

}
