package com.id.manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.id.manager.domain.User;


public interface UserRepository extends JpaRepository<User, Long> {

	@Query("SELECT u FROM User u WHERE u.email=:email")
	public  User findByEmail(@Param("email")String email);
	
	@Query("SELECT u FROM User u WHERE u.id=:id")
	public  User findById(@Param("id") Long id);
}
