package com.video.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.video.domain.MVReply;;

@Repository
public interface MVReplyDao  extends JpaRepository<MVReply, Long>{

}
