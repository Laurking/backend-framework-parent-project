package com.video.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.video.domain.MVideo;

@Repository
public interface MVideoDao extends JpaRepository<MVideo, Long> {

}
