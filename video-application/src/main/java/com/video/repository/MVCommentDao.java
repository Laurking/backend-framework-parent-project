package com.video.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.video.domain.MVComment;

@Repository
public interface MVCommentDao extends JpaRepository<MVComment, Long>{

}
