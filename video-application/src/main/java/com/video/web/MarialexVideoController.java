package com.video.web;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.video.domain.MVideo;
import com.video.service.MVideoService;

@RestController
@RequestMapping("/api/v1")
public class MarialexVideoController {

	@Autowired
	private MVideoService service;

	@RequestMapping(value = "/video/add-video", method = RequestMethod.POST)
	@ResponseBody
	public List<MVideo> createVideo(final List<MultipartFile> videoFiles) throws IOException {
		final List<MVideo> videos = this.service.addVideo(videoFiles);
		return videos;
	}

	@RequestMapping(value = "/video/all-videos", method = RequestMethod.GET)
	@ResponseBody
	public List<MVideo> findAll() {
		final List<MVideo> videos = this.service.getAllVideos();
		return videos;
	}
	
	@RequestMapping(value = "/videos/find-video/{videoId}", method = RequestMethod.GET)
	@ResponseBody
	public MVideo findVideoByPrimaryKey(@PathVariable Long videoId) {
		final MVideo video = this.service.getVideo(videoId);
		return video;
	}

}
