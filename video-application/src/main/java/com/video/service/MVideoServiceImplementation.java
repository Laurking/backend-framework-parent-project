package com.video.service;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.video.domain.MVideo;
import com.video.helperClass.FileExtensionProcessor;
import com.video.repository.MVideoDao;

@Service
public class MVideoServiceImplementation implements MVideoService {

	@Autowired
	private MVideoDao mVideoDao; 
	
	@Override
	public List<MVideo> addVideo(final List<MultipartFile> videoFiles) {
		final List<MVideo> videos = new ArrayList<MVideo>();
		videoFiles.forEach(file ->{
			final MVideo video = new MVideo();
			final FileExtensionProcessor processor = new FileExtensionProcessor();
			if(processor.isAValidVideo(file.getOriginalFilename())) {
				video.setType(FileExtensionProcessor.getFileExtension());
				video.setCreatedOn(new Timestamp(new Date().getTime()));
				video.setUpdatedOn(new Timestamp(new Date().getTime()));
				video.setFilename(file.getOriginalFilename());
				video.setSize(file.getSize());
				
				try {
					video.setSource(file.getBytes());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				this.mVideoDao.save(video);
				videos.add(video);
			}
		});
		return videos;
	}

	@Override
	public MVideo getVideo(Long videoId) {
		MVideo  video = this.mVideoDao.findOne(videoId);
		return video;
	}

	@Override
	public List<MVideo> getAllVideos() {
		List<MVideo> videos = new ArrayList<MVideo>();
		this.mVideoDao.findAll().forEach(video->{
			videos.add(video);
		});
		return videos;
	}

	@Override
	public MVideo updateVideo(MVideo newVideo) {
		MVideo video = this.mVideoDao.save(newVideo);
		return video;
	}

	@Override
	public MVideo removedVideo(Long videoId) {
		MVideo video = this.mVideoDao.findOne(videoId);
		if(video != null) {
			return video;
		}
		return video;
	}
}
