package com.video.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.video.domain.MVideo;

@Service
public interface MVideoService {
	
	public List<MVideo> addVideo(final List<MultipartFile> files);
	public MVideo getVideo(final Long videoId);
	public List<MVideo> getAllVideos();
	public MVideo updateVideo(final MVideo video);
	public MVideo removedVideo(final Long videoId);

}
